# load and evaluate a saved model
from keras.models import load_model

# load model
model = load_model('CNNmodel.h5')


#use the webcam to capture images
import cv2
import os
import serial as ser
import numpy as np 
import time

port = ser.Serial()
port.baudrate = 921600
port.port = 'COM7'



result = -1
hand_r = []
PC = -1
uC = 255
event = 0;

def Tx():
        port.open()
        #for x in range(0, 27):
         #   for y in range(0, 27):
        port.write(bytes(hand_r));
        time.sleep(1)
        #port.open()
        result = ord(port.read());
        print(result)
        port.close()
        return result

        
def Pred():
    res=model.predict(hand_r.reshape((1,28,28,1)))
    res=list(res[0])
    mx=max(res)
    print(res.index(mx))
    return res.index(mx)
    
    
xi = 150
xf = 350
yi = 150
yf = 350

cap= cv2.VideoCapture(0);
while(cap.isOpened()):
    ret, frame = cap.read()
    #print(frame.shape)
    if ret == True:
        frame = cv2.flip(frame,1)
        hand = cv2.cvtColor(frame[yi:yf,xi:xf],cv2.COLOR_BGR2GRAY)
        hand_r = cv2.resize(hand,(28,28))
        
        if uC == 255:
            stm32 = "Unknown"
        else:
            stm32 = str(chr(uC + ord('A')))
            
        frame = cv2.rectangle(frame,(yi,xi),(yf,xf),(74, 20, 222),5)
        frame = cv2.putText(frame, "PC : " + str(chr(PC + ord('A'))) + "   stm32 : " + stm32, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 1, (74, 20, 222, 255), 3)
        cv2.imshow('frame',frame)
        cv2.imshow('hand',hand)
        #cv2.imshow('hand resized',hand_r)
    else :
        print("o ow")
    k = cv2.waitKey(1)
    if  k & 0xFF == ord('q'):
        break
    elif k & 0xFF == ord('3'):
        uC = Tx()
    elif k & 0xFF == ord('0'):
        PC = Pred()
    elif k & 0xFF == ord('.'):
        PC = Pred()
        uC = Tx()

        
        
    
cv2.destroyAllWindows()
cap.release()

# TEST PROJECT : ASL RECOGNITION ON STM32

The project's goal is to test a vision based hand gesture recognition, compare it to the accelerometer based gesture 
recognition and find the pros and cons of each method.

## SENSOR 

We used a webcam in order to capture the gestures and send the data to the microcontroller.
We wrote the python script that uses openCV to control the webcam and get the result from
both the stm32 and the CNN run on the computer.

## THE CNN

We used a pretrained Convolutional Neural Network from [***Arshad Kazi's github project : Sign-Language-Recognition-***](https://github.com/Arshad221b/Sign-Language-Recognition-)
The training was done with the ***[MNIST's Sign Language DataSet](https://www.kaggle.com/datamunge/sign-language-mnist)***.

## RESULT

The model is 94% accurate, it's a little bit difficult to get the NN to recognise the signs, we feel like we have to make very precise hand figures,
but the good thing, is that, the predictions of the stm32 ar not different from the computer ones, witch means that their's no (or too little) drop in accuracy
on the stm32 and that the errors come from the model itself not from the device.

## Quick DEMO

Here's a really [quick temporary demo ](https://youtu.be/9X5oCUmwXT8)